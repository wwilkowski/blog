const path = require(`path`);
const { createFilePath } = require(`gatsby-source-filesystem`);

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;

  const blogPost = path.resolve(`./src/templates/blog-post-template.js`);
  return graphql(
    `
      {
        allDatoCmsArticle(
          sort: { fields: [date], order: DESC }
          limit: 10
          skip: 0
        ) {
          edges {
            node {
              slug
              title
            }
          }
        }
        tagsGroup: allDatoCmsArticle(limit: 2000) {
          group(field: tags) {
            fieldValue
          }
        }
      }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors;
    }

    // Create blog posts pages.
    const posts = result.data.allDatoCmsArticle.edges;

    posts.forEach((post, index) => {
      const previous =
        index === posts.length - 1 ? null : posts[index + 1].node;
      const next = index === 0 ? null : posts[index - 1].node;

      createPage({
        path: post.node.slug,
        component: blogPost,
        context: {
          slug: post.node.slug,
          previous,
          next
        }
      });
    });

    // Create blog post list pages
    const postsPerPage = 5;
    const numPages = Math.ceil(posts.length / postsPerPage);

    Array.from({ length: numPages }).forEach((_, i) => {
      createPage({
        path: i === 0 ? `/` : `/page${i + 1}`,
        component: path.resolve('./src/templates/blog-list-template.js'),
        context: {
          limit: postsPerPage,
          skip: i * postsPerPage,
          numPages,
          currentPage: i + 1
        }
      });
    });

    // Create blog sites by tags
    const tags = result.data.tagsGroup.group;
    tags.forEach(tag => {
      createPage({
        path: `/tags/${tag.fieldValue.toLowerCase()}`,
        component: path.resolve('./src/templates/posts-by-tag-template.js'),
        context: {
          limit: postsPerPage,
          tag: tag.fieldValue
        }
      });
    });
  });
};

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;

  if (node.internal.type === `allDatoCmsArticle`) {
    const value = createFilePath({ node, getNode });
    createNodeField({
      name: `slug`,
      node,
      value
    });
  }
};
