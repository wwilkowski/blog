module.exports = {
  pathPrefix: `/blog`,
  siteMetadata: {
    title: `Wojciech Glugla - Student informatyki | Frontend Developer`,
    description: `Poszerzając swoją wiedzę spostrzegłem braki w polskim internecie w tematach IT. Projekt ten ma na celu uzupełnienie tych braków.`,
    author: `@wglugla`,
    siteUrl: `https://wglugla.gitlab.io/blog/`
  },
  plugins: [
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        query: `
          {
            site {
              siteMetadata {
                title
                description
                siteUrl
                site_url: siteUrl
              }
            }
          }
        `,
        feeds: [
          {
            serialize: ({ query: { site, allDatoCmsArticle } }) => {
              return allDatoCmsArticle.edges.map(edge => {
                return Object.assign({}, edge.node.title, {
                  description: edge.node.description,
                  date: edge.node.date,
                  url: site.siteMetadata.siteUrl + edge.node.slug,
                  guid: site.siteMetadata.siteUrl + edge.node.slug,
                  custom_elements: [{ 'content:encoded': edge.node.content }]
                });
              });
            },
            query: `
            {
              allDatoCmsArticle(
                sort: { order: DESC, fields: [date] },
              ) {
                edges {
                  node {
                    title
                    slug
                    date
                    description
                    content
                  }
                }
              }
            }
          `,
            output: '/rss.xml',
            title: 'wglugla.gitlab.io/blog RSS Feed'
          }
        ]
      }
    },
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`
      }
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#FFFFFF`,
        theme_color: `#5566bb`,
        display: `minimal-ui`,
        icon: `src/images/favicon.png` // This path is relative to the root of the site.
      }
    },
    {
      resolve: `gatsby-source-datocms`,
      options: {
        apiToken: `2ebaf6b54eed6c4c8cfec7ef032cdd`
      }
    },
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`
      }
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              plugins: [
                {
                  resolve: `gatsby-remark-prismjs`,
                  options: {
                    aliases: { sh: 'bash', js: 'javascript' },
                    showLineNumbers: true
                  }
                }
              ]
            }
          }
        ]
      }
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: 'UA-148368267-1'
      }
    }
  ]
};
