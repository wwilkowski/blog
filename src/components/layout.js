import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';
import Image from '../components/image';
import './layout.css';
import CookieConsent from 'react-cookie-consent';

import Header from './header';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 66.875rem;
  margin: 0 auto;
  padding: 1.5rem;
  @media (min-width: 900px) {
    flex-direction: row;
  }
`;

const Sidebar = styled.div`
  width: 100%;
  @media (min-width: 900px) {
    width: 400px;
    padding: 1rem;
    padding-right: 2rem;
    margin-top: 1rem;
    border-right: 1px solid #c2c2c2;
  }
`;

const Footer = styled.footer`
  font-size: 0.8em;
  margin: 1rem 0;
`;

const Main = styled.main`
  width: 100%;
  @media (min-width: 900px) {
    padding: 0 4rem;
    width: 100%;
  }
`;

const Layout = ({ children }) => {
  useEffect(() => {
    const content = document.querySelector('#mainContent');
    window.scrollTo(0, content.offsetTop);
  }, []);

  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);

  return (
    <Container>
      <Sidebar>
        <Image />
        <Header siteTitle={data.site.siteMetadata.title} />
        <Footer>
          © {new Date().getFullYear()}, Built with
          {` `}
          <a href='https://www.gatsbyjs.org'>Gatsby</a>
        </Footer>
      </Sidebar>
      <Main id='mainContent'>{children}</Main>
      <CookieConsent
        style={{
          maxWidth: '800px',
          left: '50%',
          transform: 'translateX(-50%)',
          padding: '0 1rem',
          fontSize: '0.8em'
        }}
        buttonText='Jasne, rozumiem! :)'
      >
        Hej! Ta strona wykorzystuje pliki cookies!)
      </CookieConsent>
    </Container>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired
};

export default Layout;
