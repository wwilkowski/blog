import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';
import Icon from './icon';
import getIcon from '../utils/get-icon';

const Title = styled.h1`
  font-size: 1.3em;
  margin-top: 2rem;
`;

const MenuList = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
`;

const MenuLink = styled(Link)`
  color: inherit;
  font-weight: 500;
  font-size: 0.9em;
`;

const SocialsList = styled.ul`
  margin: 2rem 0;
  padding: 0;
  display: flex;
  list-style-type: none;
`;

const SocialLink = styled.a`
  padding: 0.5rem;
  margin-right: 0.5rem;
  border: 1px solid rgba(0, 0, 0, 0.1);
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50px;
`;

const Description = styled.p`
  color: #6f6f6f;
  font-size: 0.9em;
`;

const TitleLink = styled(Link)`
  color: #000000;
  text-decoration: none;
`;

const Header = ({ siteTitle }) => {
  return (
    <header>
      <Title>
        <TitleLink to='/'>Wojciech Glugla - Blog</TitleLink>
      </Title>
      <Description>Jestem studentem informatyki, zafascynowanym programowaniem i front-end developmentem.</Description>
      <Description>
        W tym projekcie chciałbym dzielić się wiedzą, której nie ma, lub jest skromnie opisana w polskim internecie.
      </Description>
      <nav>
        <MenuList>
          <li>
            <MenuLink to='/'> Artykuły </MenuLink>
          </li>
          <li>
            <MenuLink to='/o-mnie'> O mnie </MenuLink>
          </li>
        </MenuList>
      </nav>
      <SocialsList>
        <li>
          <SocialLink href='https://www.facebook.com/gluglawojciech' target='_blank'>
            <Icon icon={getIcon('facebook')} />
          </SocialLink>
        </li>
        <li>
          <SocialLink href='https://www.linkedin.com/in/wojciech-glugla/' target='_blank'>
            <Icon icon={getIcon('linkedin')} />
          </SocialLink>
        </li>
        <li>
          <SocialLink href='https://github.com/wglugla' target='_blank'>
            <Icon icon={getIcon('github')} />
          </SocialLink>
        </li>
        <li>
          <SocialLink href='https://codepen.io/wglugla/' target='_blank'>
            <Icon icon={getIcon('codepen')} />
          </SocialLink>
        </li>
      </SocialsList>
    </header>
  );
};

Header.propTypes = {
  siteTitle: PropTypes.string
};

Header.defaultProps = {
  siteTitle: ``
};

export default Header;
