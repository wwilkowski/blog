import React from 'react';

const Icon = ({ name, icon }) => (
  <svg width='18' viewBox={icon.viewBox}>
    <title>{name}</title>
    <path d={icon.path} />}
  </svg>
);

export default Icon;
