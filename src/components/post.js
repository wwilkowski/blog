import React from 'react';
import { Link } from 'gatsby';

const Post = props => {
  const { title, date, tags, description, slug } = props.data;
  return (
    <li>
      <p>
        <span>{date}</span>
        {tags.map(tag => (
          <span key={tag}>{tag}</span>
        ))}
      </p>
      <h2>{title}</h2>
      <p>{description}</p>
      <Link to={`/${slug}`}> Czytaj dalej </Link>
    </li>
  );
};

export default Post;
