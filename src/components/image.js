import React from 'react';
import styled from 'styled-components';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';

const StyledImage = styled(Img)`
  width: 75px;
  height: 75px;
  border-radius: 50px;
`;

const Image = props => {
  const data = useStaticQuery(graphql`
    query {
      profileImage: file(relativePath: { eq: "profilePhoto.jpeg" }) {
        childImageSharp {
          fluid(maxWidth: 100) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);

  return <StyledImage fluid={data.profileImage.childImageSharp.fluid} alt='' />;
};

export default Image;
