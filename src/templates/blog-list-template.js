import React from "react";
import { graphql, Link } from "gatsby";
import Layout from "../components/layout.js";
import styled from "styled-components";
import SEO from "../components/seo";

const PostList = styled.ul`
  list-style-type: none;
  margin: 2rem 0;
  padding: 0;
`;

const StyledDate = styled.span`
  margin-right: 1rem;
  font-size: 0.8em;
  text-transform: uppercase;
  color: #747474;
`;

const Tag = styled.span`
  text-transform: uppercase;
  font-size: 0.8em;
  margin-right: 1rem;
`;

const TagLink = styled(Link)`
  color: #5566bb;
  font-weight: 400;
  :hover,
  :focus {
    color: #000000;
  }
`;

const PostTitle = styled.h2`
  font-size: 1.6em;
  font-weight: 600;
  margin: 1rem 0;
`;

const StyledPost = styled.li`
  margin-bottom: 4rem;
`;

const ButtonContainer = styled.div`
  position: relative;
  padding: 2rem 0;
  bottom: 1rem;
`;

const NextLink = styled(Link)`
  position: absolute;
  bottom: 0;
  right: 0;
`;

const PrevLink = styled(Link)`
  position: absolute;
  bottom: 0;
  left: 0;
`;

const Description = styled.p`
  font-size: 0.9em;
  color: #6f6f6f;
`;

const StyledLink = styled(Link)`
  color: #3e6fb5;
  text-decoration: none;
  font-size: 0.9em;
`;

const TitleLink = styled(Link)`
  color: #000000;
  text-decoration: none;
  :hover {
    color: #000000;
    text-decoration: underline;
  }
`;

const PostInfo = styled.p`
  margin: 0;
  font-size: 0.9em;
`;

export default class BlogList extends React.Component {
  render() {
    const posts = this.props.data.allDatoCmsArticle.edges;
    const { currentPage, numPages } = this.props.pageContext;
    const isFirstPage = currentPage === 1;
    const isLastPage = currentPage === numPages;
    const prevPage = currentPage - 1 === 1 ? `/` : `page${currentPage - 1}`;
    const nextPage = `page${currentPage + 1}`;
    return (
      <Layout>
        <SEO title="Wojciech Glugla - Blog" />
        <PostList>
          {posts.map(post => {
            const { slug, date, tags, description, title } = post.node;
            return (
              <StyledPost key={slug}>
                <PostInfo>
                  <StyledDate>{date}</StyledDate>
                  {tags.map(tag => (
                    <Tag key={tag}>
                      <TagLink to={`tags/${tag.toLowerCase()}`}>{tag}</TagLink>
                    </Tag>
                  ))}
                </PostInfo>
                <PostTitle>
                  <TitleLink to={`/${slug}`}>{title}</TitleLink>
                </PostTitle>
                <Description>{description}</Description>
                <StyledLink to={`/${slug}`}> Czytaj więcej </StyledLink>
              </StyledPost>
            );
          })}
        </PostList>
        <ButtonContainer>
          {!isFirstPage && (
            <PrevLink to={`/${prevPage}`}> Poprzednia strona </PrevLink>
          )}
          {!isLastPage && (
            <NextLink to={`/${nextPage}`}> Następna strona </NextLink>
          )}
        </ButtonContainer>
      </Layout>
    );
  }
}

export const blogListQuery = graphql`
  query blogListQuery($skip: Int!, $limit: Int!) {
    allDatoCmsArticle(
      sort: { fields: [date], order: DESC }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          date(formatString: "DD MMMM YYYY", locale: "pl-PL")
          tags
          title
          description
          slug
        }
      }
    }
  }
`;
