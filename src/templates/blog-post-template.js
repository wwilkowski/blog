import React from "react";
import { graphql, Link } from "gatsby";
import ReactMarkdown from "react-markdown";
import styled from "styled-components";
import Prism from "prismjs";
import SEO from "../components/seo";
import { DiscussionEmbed } from "disqus-react";

require("prismjs/components/prism-javascript");
require("prismjs/components/prism-haml");
require("prismjs/components/prism-bash");
require("prismjs/components/prism-jsx");
require("prismjs/components/prism-json");
require("prismjs/components/prism-graphql");

const StyledPost = styled.div`
  padding: 2rem 1rem;
  @media (min-width: 900px) {
    max-width: 800px;
    margin: 0 auto;
    text-align: justify;
  }
`;

const StyledLink = styled(Link)`
  color: #3e6fb5;
  text-decoration: none;
  font-weight: 300;
  font-size: 0.88em;
`;

const StyledSign = styled.p`
  color: #6f6f6f;
  font-size: 0.77em;
  margin: 1rem 0;
  ul {
    max-width: 300px;
    display: flex;
    list-style: none;
    margin: 0;
    padding: 0;
    justify-content: space-between;
    a {
      text-decoration: none;
      color: inherit;
    }
  }
`;

const StyledContent = styled.div`
  line-height: 1.8;
`;

const StyledImage = styled.img`
  padding: 3rem 0;
`;

const StyledQuote = styled.blockquote`
  color: #777;
  margin: 1.5rem 0;
  padding: 0.5rem 0 0.5rem 1rem;
  position: relative;
  border-left: 0.25em solid #ddd;
`;

const StyledFooter = styled.footer`
  border-top: 1px solid #aaa;
  padding-top: 2rem;
`;

const SocialsList = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
  display: flex;
  a {
    margin-right: 1rem;
  }
`;

const FooterInfo = styled.div`
  margin-top: 1.2rem;
`;

const TagList = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
  display: flex;
  margin-bottom: 2rem;
`;

const Tag = styled.span`
  text-transform: uppercase;
  margin-right: 1rem;
`;

const TagLink = styled(Link)`
  color: #000000;
  font-weight: 400;
`;

const StyledList = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
  width: 100%;
  display: flex;
  margin-bottom: 2rem;
  li {
    margin-right: 2rem;
  }
`;

const StyledCode = styled.code`
  background: #f1f1f1;
  padding: 0.3rem;
  margin: 0 0.2rem 0 0;
  font-size: 1rem;
`;

function image(props) {
  return (
    <StyledImage src={props.src} alt="">
      {props.children}
    </StyledImage>
  );
}

function blockquote(props) {
  return <StyledQuote>{props.children}</StyledQuote>;
}

function paragraph(props) {
  return <p>{props.children}</p>;
}

function inlineCode(props) {
  return <StyledCode>{props.children}</StyledCode>;
}

function code(props) {
  if (!props.language) {
    return (
      <pre>
        <code dangerouslySetInnerHTML={{ __html: props.value }} />
      </pre>
    );
  } else {
    const html = Prism.highlight(props.value, Prism.languages[props.language]);
    const cls = "language-" + props.language;
    return (
      <pre className={cls}>
        <code dangerouslySetInnerHTML={{ __html: html }} className={cls} />
      </pre>
    );
  }
}

export default class BlogPost extends React.Component {
  render() {
    const post = this.props.data.datoCmsArticle;
    const { id, title, date, content, tags, slug } = post;
    const disqusShortname = "wglugla";
    const disqusConfig = {
      identifier: id,
      title: title
    };

    return (
      <StyledPost>
        <SEO title={title} description={post.description} url={post.slug} />
        <StyledList>
          <li>
            <StyledLink to="/"> Wszystkie artykuły </StyledLink>
          </li>
          <li>
            <StyledLink to="/o-mnie"> O mnie </StyledLink>
          </li>
        </StyledList>
        <article>
          <h1>{title} </h1>
          <StyledSign>
            <ul>
              <li>
                <Link to="/o-mnie"> Wojciech Glugla </Link>
              </li>
              <li>
                <span> · </span>
              </li>
              <li>
                <p>{date}</p>
              </li>
              <li>
                <span> · </span>
              </li>
              <li>
                <a href="#komentarze"> Skomentuj </a>
              </li>
            </ul>
          </StyledSign>
          <StyledContent>
            <ReactMarkdown
              source={content}
              renderers={{
                image: image,
                code: code,
                blockquote: blockquote,
                paragraph: paragraph,
                inlineCode: inlineCode
              }}
            />
            <div>
              <span> Tagi artykułu: </span>
              <TagList>
                {tags.map(tag => (
                  <Tag key={tag}>
                    <TagLink to={`tags/${tag}`}>{tag}</TagLink>
                  </Tag>
                ))}
              </TagList>
            </div>
            <StyledFooter>
              <h2>Czy uważasz ten artykuł za wartościowy?</h2>
              <p>
                Podziel się ze mną swoją opinią, będę bardzo wdzięczny za
                wszelkie uwagi!
              </p>
              <SocialsList>
                <li>
                  <a href="http://m.me/gluglawojciech"> Messenger </a>
                </li>
                <li>
                  <a href="mailto: gluglawojciech@gmail.com">
                    gluglawojciech@gmail.com
                  </a>
                </li>
              </SocialsList>
              <DiscussionEmbed
                shortname={disqusShortname}
                config={disqusConfig}
              />
              <FooterInfo>
                © {new Date().getFullYear()}, Built with
                {` `}
                <a href="https://www.gatsbyjs.org">Gatsby</a>
              </FooterInfo>
            </StyledFooter>
          </StyledContent>
        </article>
      </StyledPost>
    );
  }
}

export const postBySlug = graphql`
  query BlogPostBySlug($slug: String) {
    site {
      siteMetadata {
        title
        author
      }
    }
    datoCmsArticle(slug: { eq: $slug }) {
      id
      title
      date(formatString: "DD MMMM YYYY", locale: "pl-PL")
      description
      content
      tags
      slug
    }
  }
`;
