import React from 'react';

import Layout from '../components/layout';
import SEO from '../components/seo';
import styled from 'styled-components';

const Container = styled.div`
  margin: 4rem 0;
`;
const StyledTitle = styled.h1`
  font-size: 1.8em;
`;

const StyledP = styled.p`
  font-size: 0.9em;
`;

const NotFoundPage = () => (
  <Layout>
    <SEO title='O mnie' />
    <Container>
      <StyledTitle> O mnie </StyledTitle>
      <StyledP>
        Jestem studentem trzeciego roku informatyki inżynierskiej na UMK w
        Toruniu. Od grudnia 2016 roku interesuję się programowaniem, zwracając
        szczególną uwagę na front-end development.
      </StyledP>
      <StyledP>
        Od zawsze staram się jednak nie ograniczać w technologiach. Wybór
        kierunku studiów pozwolił mi opanować na całkiem niezłym poziomie języki
        C, C++, C#.
      </StyledP>
      <StyledP>
        Blog ten powstał z kilku powodów. Od zawsze chciałem pisać, gdyż wydaje
        mi się, że mam tę "lekkość" w pisaniu więc projekt ten będzie dla mnie
        wyzwaniem i możliwością faktycznego sprawdzenia swoich umiejętności.
        Poza tym w trakcie swojej edukacji (zarówno tej na studiach jak i tej
        własnej) często spotykam się z niedoborem materiałów w języku polskim.
        Mimo ogólnego trendu na artykuły w języku angielskim uważam, że warto
        czasami napisać coś po polsku.
      </StyledP>
      <StyledP>
        {' '}
        Przez kilka najbliższych miesięcy kluczową dla mnie sprawą będzie
        feedback. Nie krępuj się odezwać do mnie i podzielić się swoimi uwagami,
        zarówno tymi pozytywnymi, jak i negatywnymi. Nic tak nie buduje
        człowieka i jego doświadczenia, jak konstruktywna krytyka!{' '}
      </StyledP>
    </Container>
  </Layout>
);

export default NotFoundPage;
