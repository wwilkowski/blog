import ICONS from '../shared/icons';

const getIcon = name => {
  let icon;
  switch (name) {
    case 'facebook':
      icon = ICONS.FACEBOOK;
      break;
    case 'linkedin':
      icon = ICONS.LINKEDIN;
      break;
    case 'github':
      icon = ICONS.GITHUB;
      break;
    case 'codepen':
      icon = ICONS.CODEPEN;
      break;
    default:
      icon = {};
      break;
  }

  return icon;
};

export default getIcon;
