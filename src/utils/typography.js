import Typography from 'typography';
import theme from 'typography-theme-noriega';

theme.baseFontSize = '16px';

const typography = new Typography(theme);

export default typography;
