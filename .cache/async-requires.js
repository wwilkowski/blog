// prefer default export if available
const preferDefault = m => m && m.default || m

exports.components = {
  "component---src-templates-blog-post-template-js": () => import("/home/wglugla/Projects/blog/src/templates/blog-post-template.js" /* webpackChunkName: "component---src-templates-blog-post-template-js" */),
  "component---src-templates-blog-list-template-js": () => import("/home/wglugla/Projects/blog/src/templates/blog-list-template.js" /* webpackChunkName: "component---src-templates-blog-list-template-js" */),
  "component---src-templates-posts-by-tag-template-js": () => import("/home/wglugla/Projects/blog/src/templates/posts-by-tag-template.js" /* webpackChunkName: "component---src-templates-posts-by-tag-template-js" */),
  "component---cache-dev-404-page-js": () => import("/home/wglugla/Projects/blog/.cache/dev-404-page.js" /* webpackChunkName: "component---cache-dev-404-page-js" */),
  "component---src-pages-404-js": () => import("/home/wglugla/Projects/blog/src/pages/404.js" /* webpackChunkName: "component---src-pages-404-js" */),
  "component---src-pages-o-mnie-js": () => import("/home/wglugla/Projects/blog/src/pages/o-mnie.js" /* webpackChunkName: "component---src-pages-o-mnie-js" */)
}

