const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---src-templates-blog-post-template-js": hot(preferDefault(require("/home/wglugla/Projects/blog/src/templates/blog-post-template.js"))),
  "component---src-templates-blog-list-template-js": hot(preferDefault(require("/home/wglugla/Projects/blog/src/templates/blog-list-template.js"))),
  "component---src-templates-posts-by-tag-template-js": hot(preferDefault(require("/home/wglugla/Projects/blog/src/templates/posts-by-tag-template.js"))),
  "component---cache-dev-404-page-js": hot(preferDefault(require("/home/wglugla/Projects/blog/.cache/dev-404-page.js"))),
  "component---src-pages-404-js": hot(preferDefault(require("/home/wglugla/Projects/blog/src/pages/404.js"))),
  "component---src-pages-o-mnie-js": hot(preferDefault(require("/home/wglugla/Projects/blog/src/pages/o-mnie.js")))
}

